﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "NewAttributesObject", menuName = "AttributesObject")]
public class AttributesObject : ScriptableObject
{
    public Attributes attributes;

}
